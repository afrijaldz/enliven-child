<?php
/**
 * Template Name: Page Depan
 */

get_header(); ?>

<!-- <section class="row section-1">
  <div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 images">
      <img src="images/image-top.png" >
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text">
  			Maksimalkan profit bisnis Anda dengan
  			<b>Aplikasi Kasir lengkap &amp; praktis</b><br>
  			<p>Cara Smart &amp; Mudah Untuk Menjalankan Bisnis Anda</p>
  			<a class="button coba-gratis">COBA GRATIS SEKARANG</a>
        <a href="#" class="btnpopup play" data-toggle="modal" data-target="#video"><img src="images/video1.svg" class="video"></a>
        <a href="#" target="_blank">
          <img data-rjs="2" src="images/google_play.png" class="google_play">
        </a>
    </div>
  </div>
  </div>
</section>

<section class="row section-2">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="wrapper">
			<h2>Cocok untuk segala jenis bisnis</h2>
			<p>Pilih jenis bisnis Anda untuk melihat bagaimana Noota dapat bekerja untuk Anda.</p>
      <div class="rounded-icon">
        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
          <a href="categories-food-beverages">
            <div class="box-icon">
              <img data-rjs="2" src="images/Coffee Shop.png" class="icon-coffee">
            </div>
            Coffee shop
          </a>
        </div>
        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
          <a href="categories-food-beverages">
            <div class="box-icon">
              <img data-rjs="2" src="images/Retail Store.png" class="icon-coffee">
            </div>
            Retail Store
          </a>
        </div>
        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
          <a href="categories-food-beverages">
            <div class="box-icon">
              <img data-rjs="2" src="images/Barbershop.png" class="icon-coffee">
            </div>
            Barbershop
          </a>
        </div>
        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
          <a href="categories-food-beverages">
            <div class="box-icon">
              <img data-rjs="2" src="images/Bazaar - Event.png" class="icon-coffee">
            </div>
            Bazaar - Expo
          </a>
        </div>
        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
          <a href="categories-food-beverages">
            <div class="box-icon">
              <img data-rjs="2" src="images/Fashion Boutique.png" class="icon-coffee">
            </div>
            Fashion Boutique
          </a>
        </div>
        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
          <a href="categories-food-beverages">
            <div class="box-icon">
              <img data-rjs="2" src="images/Restaurant.png" class="icon-coffee">
            </div>
            Restaurant
          </a>
        </div>
        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
          <a href="categories-food-beverages">
            <div class="box-icon">
              <img data-rjs="2" src="images/Franchise.png" class="icon-coffee">
            </div>
            Franchise
          </a>
        </div>
        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
          <a href="categories-food-beverages">
            <div class="box-icon">
              <img data-rjs="2" src="images/Food Truck.png" class="icon-coffee">
            </div>
            Food Truck
          </a>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="row section-3">
  <div class="col-lg-12">
    <div class="wrapper">
      <h1 class="headline">Mulai berjualan dengan mudah</h1>
      <p class="tagline">Apapun jenis bisnis Anda, mulai dari toko retail, restoran, cafe, butik, salon kecantikan atau bahkan popup store, Noota merupakan
        aplikasi kasir berbasis online (cloud) yang akan membantu meningkatkan produktivitas Anda sebagai pebisnis.</p>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 kiri">
      <h3>Satu aplikasi, Banyak solusi</h3>
      <ul>
				<li>Mudah digunakan fitur lengkap <img src="images/list-style-2.png"></li>
				<li>Dapat berjalan di berbagai device Android <img src="images/list-style-2.png"></li>
				<li>Sistem backoffice yang handal dan lengkap <img src="images/list-style-2.png"></li>
				<li>Investasi yang murah dan smart <img src="images/list-style-2.png"></li>
			</ul>
    </div>
    <div class="col-lg-7 col-md-6 col-sm-6 col-xs-12 kanan">
      <img src="images/POS-2.png" alt="">
    </div>
  </div>
</section>

<section class="row section-4">
  <div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
      <img src="<?php echo wp_get_attachment_url( 92 ); ?>" alt="">
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
      <img src="<?php echo wp_get_attachment_url( 93 ); ?>" alt="">
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
      <img src="<?php echo wp_get_attachment_url( 94 ); ?>" alt="">
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
      <img src="<?php echo wp_get_attachment_url( 95 ); ?>" alt="">
    </div>
  </div>
</section> -->

<section class="row section-5">
  <div class="wrapper">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <h1>Aplikasi kasir yang simpel, fleksibel dan mudah digunakan</h1>
      <p>Noota menangani segala aktivitas bisnis Anda mulai dari transaksi pembayaran, sistem manajemen stok, laporan harian penjualan, hingga membantu kebutuhan marketing. Kenali semua fitur lebih dalam untuk membantu mengembangkan bisnis Anda.</p>

      <div class="col-lg-6 kiri"> <!-- Kiri -->
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="img-box"><img src="images/Pembayaran.png"></div>
          </div>
          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
            <div class="text-box">
  						<h3>Terima pembayaran dengan mudah</h3>
  						<p>Mulai dari pembayaran tunai, kartu debit, hingga kartu kredit  dapat terima oleh aplikasi Noota.</p>
  					</div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="img-box"><img src="images/Akses Online.png"></div>
          </div>
          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
            <div class="text-box">
              <h3>Akses online dan offline</h3>
  						<p>Noota dapat bekerja sangat fleksibel, dengan internet maupun tanpa internet Noota akan tetap bekerja.</p>
  					</div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="img-box"><img src="images/terhubung.png"></div>
          </div>
          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
            <div class="text-box">
  						<h3>Tetap terhubung pelanggan Anda</h3>
  						<p>Ketahui apa yang diinginkan konsumen Anda lewat fitur manajemen konsumen. Bangun relasi Anda lebih dalam dengan bantuan Noota Marketing App.</p>
  					</div>
          </div>
        </div>
      </div>

      <div class="col-lg-6 kanan">  <!-- Kanan -->
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="img-box"><img src="images/laporan.png"></div>
          </div>
          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
            <div class="text-box">
              <h3>Dashboard dan laporan lengkap</h3>
  						<p>Noota menyajikan data vital mengenai bisnis Anda secara real-time dan mudah. Dapatkan insight bisnis Anda lebih dalam.</p>
  					</div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div class="img-box"><img src="images/multi cabang.png"></div>
          </div>
          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
            <div class="text-box">
  						<h3>Multi cabang</h3>
  						<p>Atur satu outlet ataupun banyak cabang sekaligus hanya dengan beberapa klik. Kendalikan bisnis tanpa harus datang ke gerai.</p>
  					</div>
          </div>
        </div>
        <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
          <div class="img-box"><img src="images/stock.png"></div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
          <div class="text-box">
						<h3>Manajemen stok</h3>
						<p>Kontrol stok di semua cabang bisnis Anda setiap hari. Anda juga dapat mengelola stok bahan baku dan meng-updatenya dari mana saja dan otomatis.</p>
					</div>
        </div>
        </div>
      </div>

    </div>
  </div>
</section>

<!-- <section class="row section-6">
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 kiri">
  </div>
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 kanan">
    <div class="wrapper">
      <span>new</span>
      <h1>Noota Waiter app</h1>
      <p>Jangan buat pelanggan Anda menunggu! Kini pencatatan pesanan di gerai Anda, tidak perlu lagi menggunakan pena dan kertas. Dengan menggunakan aplikasi tambahan Noota Waiter App (Aplikasi Pelayan) pesanan akan secara otomatis terhubung dengan kasir dan dapur.</p>
      <a class="button coba-gratis" href="#">Pelajari lebih lanjut</a>
    </div>
  </div>
</section>

<section class="row section-7">
  <div class="col-lg-12">
    <div class="wrapper">
      <div class="box-custom">
        <span>new</span>
        <h2>Noota <b>Marketing App</b></h2>
      </div>
      <p>Wujudkan mimpi Anda untuk membuat aplikasi toko Anda sendiri. Dengan Noota Marketing App kini Anda dapat mempunyai app untuk toko Anda sendiri secara mudah, cepat dan terjangkau. Marketing App berfungsi untuk memudahkan komunikasi dengan pelanggan, menampilkan promo-promo terbaru atau bahkan informasi produk-produk terbaru di toko Anda.</p>
      <p>Tertarik untuk mempelajari lebih lanjut? <a href="#">Hubungi kami </a></p>
    </div>
  </div>
  <div class="col-lg-12">
    <img src="<?php echo wp_get_attachment_url( 121 ); ?>" alt="">
  </div>
</section>

<section class="row section-8">
    <div class="col-lg-5 col-sm-5 col-md-5">
      <img data-rjs="2" src="images/img-38.png" class="owner">
      <div class="box-text">
				<img data-rjs="2" src="images/logo27-2.png" class="logo">
				<span>Kedai 27 menggunakan Noota</span>
				<p>Kedai 27 merupakan ‘coffeeshop’ lokal yang sedang berkembang. Memiliki beberapa cabang di Indonesia yang telah menggunakan Noota. Pelajari bagaimana mereka menjual dan mengelola produk mereka dengan bantuan Noota.</p>
				<a href="case-study" class="button custom">VIEW CASE STUDY &rarr;</a>
			</div>
    </div>
    <div class="col-lg-5 col-sm-5 col-md-5">
      <img data-rjs="2" src="images/img-39.png" class="owner">
      <div class="box-text">
				<img data-rjs="2" src="images/logodecutz.png" class="logo">
				<span>De’ Cutz Barbershop menggunakan Noota</span>
				<p>Pelajari dari Edo, seorang owner sebuah barbershop di daerah Surabaya ini menggunakan Noota untuk memonitor dan mengembangkan bisnisnya dengan rinci kapanpun dan dimanapun dia berada.</p>
				<a href="case-study-1" class="button custom">VIEW CASE STUDY &rarr;</a>
			</div>
    </div>
</section>

<section class="row section-9">
  <div class="wrapper">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
      <h1>Why everyone &hearts; Noota</h1>
      <p>Noota telah dipercaya oleh banyak pelaku bisnis untuk meningkatkan bisnisnya dengan cara yang smart</p>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
      <img src="images/logo-patron.png" class="logo">
      <p>“Saya puas karena Noota simpel secara tampilan dan penggunaan. Staf saya pun mudah mengerti.”</p>
      <span>Andhika Ramdhan Fauzie</span>
      <span>Owner Patron Barbershop</span>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12  ">
      <img data-rjs="2" src="images/logo-taiyaki.png" class="logo">
      <p>“Aplikasi atau hardware yg cukup mudah dikelola, dan juga pelayanan penjualan after sales yg selalu helpful.”</p>
      <span>Bobby Surya D</span>
      <span>Owner Taiyaki-Den</span>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
      <img data-rjs="2" src="images/logo-martabakbro.png" class="logo">
      <p>“Noota sangat berguna sebagai program kasir untuk retail, praktis, user friendly dan sudah bisa memenuhi kebutuhan system di retail business.”</p>
      <span>Ilham</span>
      <span>Owner Martabak Bro</span>
    </div>
  </div>
</section>

<section class="row section-10">
  <div class="col-lg-12 col-sm-12 col-xs-12 ">
    <h1>Tingkatkan bisnis Anda sekarang</h1>
    <p>Hanya membutuhkan waktu beberapa menit saja untuk bergabung. Tidak ada kontrak. Coba gratis selama 14 hari</p>
    <a class="button" href="#">Coba gratis</a>
    <h3>Atau hubungi kami</h3>
    <h3>
      <i class="fa fa-phone" aria-hidden="true"></i> 0852 2880 2828  <br>
      <i class="fa fa-whatsapp" aria-hidden="true"></i> 0852 2880 2828
    </h3>
    <h4>Jam operasional: Senin-Jumat pukul 09.00-18.00</h4>
  </div>
</section> -->

<?php
  get_footer();
?>
