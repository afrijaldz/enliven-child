<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Enliven
 */

?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<div class="row">
				<div class="wrapper">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<label>Noota POS</label>
						<ul>
							<li class="sub-menu"><a href="#">Penjelasan Fitur</a></li>
							<li class="sub-menu"><a href="#">Dashboard Laporan</a></li>
							<li class="sub-menu"><a href="#">Kelola Multi Outlet</a></li>
							<li class="sub-menu"><a href="#">Studi Kasus</a></li>
							<li class="sub-menu"><a href="#">Hardware</a></li>
							<li class="sub-menu"><a href="#">Harga</a></li>
							<li class="sub-menu"><a href="#">Back Office Login</a></li>
							<li class="sub-menu"><a href="#">Blog</a></li>
						</ul>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<label>Business Solution</label>
						<ul>
							<li class="sub-menu"><a href="#">Coffee Shop</a></li>
							<li class="sub-menu"><a href="#">Retail Store</a></li>
							<li class="sub-menu"><a href="#">Barbershop</a></li>
							<li class="sub-menu"><a href="#">Bazaar/Expo</a></li>
							<li class="sub-menu"><a href="#">Fashion Boutique</a></li>
							<li class="sub-menu"><a href="#">Restaurant</a></li>
							<li class="sub-menu"><a href="#">Franchise</a></li>
							<li class="sub-menu"><a href="#">Food Truck</a></li>
						</ul>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<label>Bantuan</label>
						<ul>
							<li class="sub-menu"><a href="#">Petunjuk Lengkap</a></li>
							<li class="sub-menu"><a href="#">Manual Lengkap</a></li>
						</ul>
						<label>About Us</label>
						<ul>
							<li class="sub-menu"><a href="#">Team</a></li>
							<li class="sub-menu"><a href="#">Privacy Policy</a></li>
							<li class="sub-menu"><a href="#">Term of Service</a></li>
						</ul>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<img style="width: 150px; background: #fff; float: inherit; margin-left: 20px;" src="<?php echo wp_get_attachment_url( 168 ); ?>" alt="">
						<ul>
							<li class="sub-menu"><a href="#">Ngoto Baru Vila No. 3</a></li>
							<li class="sub-menu"><a href="#">Jl. Imogiri Barat km. 6.5, Ngoto, Sewon</a></li>
							<li class="sub-menu"><a href="#">Bantul, Yogyakarta 55187</a></li>
							<li class="sub-menu contact"><i class="fa fa-whatsapp"></i><label> 08528802828</label></li>
				      <li class="sub-menu contact"><i class="fa fa-phone"></i><label> 08528802828</label></li>
							<li class="sub-menu contact"><a href="mailto:info@noota.id"><i class="fa fa-envelope-o"></i><label>info@noota.id</label></a></li>
							<li>
								<a target="_blank" href="https://facebook.com/noota.id"><i class="fa fa-facebook sm"></i></a>
								<a href="#"><i class="fa fa-twitter sm"></i></a>
								<a href="#"><i class="fa fa-instagram sm"></i></a>
								<a href="#"><i class="fa fa-google sm"></i></a>
								<a href="#"><i class="fa fa-linkedin sm"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</footer>

<?php wp_footer(); ?>

</body>
</html>
