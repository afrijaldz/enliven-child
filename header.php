<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Enliven
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<header id="masthead" class="site-header <?php echo enliven_header_class(); ?>" role="banner">
    <div class="container">
      <div class="row">
				<!-- Navigation Logo -->
				<div class="col-xs-10 col-md-4 col-lg-4">
          <!-- <div class="site-branding"> -->
					<div class="site-logo">
						<div class="logo1">
	            <a class="navbar-brand " href="<?php echo home_url(); ?>"><img src="images/Noota2.png"> </a>
	          </div>
	          <div class="logo2">
	            <a class="navbar-brand" href="<?php echo home_url(); ?>"> </a>
	            <span class="phone">
	              <i class="fa fa-2x fa-phone"> 085228802828</i>
	            </span>
	          </div>
					</div>
          <!-- </div> -->
        </div>
				<!--/ Navigation Logo -->
				<!-- Navigation menu -->
        <div class="col-xs-2 col-md-8 col-lg-8">
          <nav id="site-navigation" class="main-navigation" role="navigation">
            <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
          </nav><!-- #site-navigation -->
          <a href="#" class="navbutton" id="main-nav-button"></a>
        </div>
				<!-- / Navigation Menu -->
      </div><!-- .row -->
    </div><!-- .container -->
	</header><!-- #masthead -->
  <div class="responsive-mainnav-outer">
  	<div class="responsive-mainnav container"></div>
	</div>

  <?php do_action('enliven_after_header'); ?>

  <?php enliven_header_background(); ?>

	<div id="content" class="site-content">
