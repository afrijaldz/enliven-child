<?php

/*
  ===============
  get parent style
  ===============
*/

function my_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_script('jquery2.2.4', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js');
    wp_enqueue_style('bootsrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
    wp_enqueue_style('fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

/*
  ===============
  custom post type
  ===============
*/

function awesome_custom_post_type(){
  $labels = [
    'name' => 'Section',
    'singular_name' => 'Section',
    'add_new' => 'Add Section',
    'all_items' => 'All Items',
    'add_new_item' => 'Add Item',
    'edit_item' => 'Edit Item',
    'new_item' => 'New Item',
    'view_item' => 'View Item',
    'search_item' => 'Search Section',
    'not_found' => 'No items found',
    'not_found_in_trash' => 'No items found in trash',
    'parent_item_colon' => 'Parent Item'
  ];
  $args = [
    'labels' => $labels,
    'public' => true,
    'has_archive' => true,
    'publicly_queryable' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'supports' => [
      'title',
      'editor',
      'excerpt',
      'thumbnail',
      'revisions',
    ],
    'taxonomies' => ['category', 'post_tag'],
    'menu_position' => 3,
    'exclude_from_search' => false
  ];
  register_post_type('section', $args);
}
add_action('init', 'awesome_custom_post_type' );
